# COSMAC MICROBOARD BUFFERED PROTOBOARD INSTRUCTIONS - CDP18S704

__Version 1.0__


Thank you for purchasing the COSMAC System 1 Buffered Protoboard Microboard. This PCB allows for conveniently prototyping or building one-off RCA Microboards. Originally designed for the 64K memory card, the board simplifies interfacing circuitry required in a multi-board system where data bus ownership must be arbitrated between the CPU and other boards.

Features:

* Tri-state data bus buffering to prevent bus contention
* Address bus buffering and latching for upper 8 bits
* TPA, MRD, MWR and RUN buffered inputs
* Additional buffering for 4 user-selectable signal lines is provided
* All buffered signals are arranged on .100" centers for either a header or solder connection

The capacity of the board is as follows:
* 25 14-pin DIP's
* 20 16-pin DIP's
* 15 24-pin or 28-pin DIP's
* or a combination of the above

A grid of pads on .100" centers are provided at the top of the board for connectors, headers or discrete components as may be needed.

The edge connector is silk screened on both sides with the 1802 signal names for easy identification.


## Circuit Description

In RCA CPU designs, 1802 signals are tied directly to the bus and are not buffered or isolated. Thus peripheral Microboards need to uniquely select/deselect themselves within the system.

U1 provides bi-directional data bus buffering and selection controls in combination with TPA and MRD.  MRD is used to select whether the buffers are A->B or B->A. TPA and the board-generated CS signal determine when the output buffers are enabled versus in the high-impedence isolated state.

Data bus contention is avoided by disconnecting the card bus from the system bus via U1 when CS is not asserted. The logic for determining the active board state is left to the designer and to be implemented in the prototyped circuit. This could be any number of schemes such as N-line decoding, memory mapped addressing, other 1802 signals, or external inputs.

U2 provides buffering for MRD, MWR, TPA and RUNU, the remainder of the I/O are unused. U3 provides buffering for the address lines A0-A7. U4 latches the address lines when TPA is asserted to provide full 16-bit addressing. RUNU and A15 are OR'd via U6 to select the upper 32K boundary (at 0X8000) to enable selection of a ROM device when the system enters run mode by pressing the RUNU push button. 

It is likely that some lines buffered are not needed or others that are needed are not presently buffered. A certain amount of hand wiring for CPU lines and possibly rerouting of PCB traces is to be expected depending on the end use. 

## PCB

The PCB is double-sided FR4 material and plated in electro-less nickel immersion gold (ENIG). This provides good protection from oxidation on the edge card fingers.

You will find best results working on this board using a variable temperature soldering station and a micro soldering tip. In the need of desoldering, a powered vacuum desoldering gun such as the Hakko FR-301 is highly recommended.

> **NOTE:**
The solder pads on the board have very small profiles and are easily damaged by too high or prolonged heat which will cause them to lift from the board.


## Assembly

1. Wash the board in warm water and mild detergent to remove any oils and contamination from the plated surfaces. Blot dry between two clean paper towels and allow to fully dry.
1. Add the filter capacitors (22uF electolytic).


## Checkout

Power the board and verify that power and ground voltages are correct with respect to the labeled connections.


## Legal Disclaimer, Warranty and Customer Support ##

Use of this product is without warranty of merchantability of fitness for any purpose other than learning and enjoyment!

I support and stand by my products. Email support is available should you have any questions or issues. PCB's are warranted against defects in manufacture and will be replaced free of charge. Kit IC's have a 30-day DOA warranty from date of receipt. If you encounter any problems or have suggestions please contact me.

I sincerely hope you enjoy this retro computing creation and thanks again for your support!

Scott Pack/packtronix

[somaspack@yahoo.com]

<p style="page-break-after: always;">
</p>


# Appendix I - Schematic

## COSMAC Microboard Signal Buffering

<p style="page-break-after: always;">
    <a href="schematics/cosmac_data_and_addess_buffering.svg"><img src="schematics/cosmac_data_and_addess_buffering.svg" width="750" style="border:1px solid black"/></a>
</p>

## COSMAC Microboard Bus

<p style="page-break-after: always;">
    <a href="schematics/cosmac_expansion_44edge.svg"><img src="schematics/cosmac_expansion_44edge.svg" width="500" style="border:1px solid black"/></a>
</p>

# Appendix II - PCB

<p style="page-break-after: always;">
    <a href="schematics/buffered_protoboard_pcb.svg"><img src="schematics/buffered_protoboard_pcb.svg" width="475" style="border:1px solid black"/></a>
</p>