# COSMAC SYSTEM 1 64K RAM/ROM MEMORY MICROBOARD INSTRUCTIONS - CDP18S638

__Version 1.0__

Thank you for purchasing the COSMAC System 1 64K RAM/ROM Microboard CDP18S638!

This is a retro-computing project inspired by RCA's Microboard series of products produced in the late 1970's and 1980's. Inspiration for this project is based on the RCA's CDP18S626 *RCA COSMAC Microboard 32/64-Kilobyte EPROM/ROM/RAM* and CDP18S629 *RCA COSMAC Microboard 32-Kilobyte RAM* Microboard. 

This Microboard supplies 64K of RAM and ROM in an 1802-based Microboard architecture system. Two banks are provided:

* 32K RAM with a base address located at either 0x0000 or 0x8000. Uses 62256 type CMOS Static RAM devices.
* 32K RAM/ROM in 4 x 8K segments configurable within the 32K address block, with a base address at 0x0000 or 0x8000. Uses 2732 EPROM, 28C32 EEPROM or 6264 Static RAM type devices.

The board is compatible with the System 1 CPU board and provides circuitry for address/data bus contention with other boards in a system.

## PCB ##

The PCB is double-sided FR4 material and plated in electro-less nickel immersion gold (ENIG). This provides good protection from oxidation on the edge card fingers.

You will find best results working on this board using a variable temperature soldering station and a micro soldering tip. In the need of desoldering, a powered vacuum desoldering gun such as the Hakko FR-301 is highly recommended.

> **NOTE:**
The solder pads on the board have very small profiles and are easily damaged by too high or prolonged heat which will cause them to lift from the board.

## Circuit Description ##

### Data Bus Buffering

U1 provides bi-directional data bus buffering and selection controls in combination with TPB and MW. Data bus contention is avoided by disconnecting the card bus from the system bus via U1 when CS1 or CS2 are not asserted. U2 provides buffering for MRD, MWR, TPA and RUNU, the remainder of the I/O are unused. U3 provide buffering for the address lines A0-A7. U4 latches the address lines on TPA to provide full 16-bit addressing. RUNU and A15 are OR'd via U6 to select the upper 32K boundary (at 0X8000) to enable selection of a ROM device when the system enters run mode by pressing the RUNU push button. Memory output enable (OE) is handled by OR'ing TPA and MRD via U6.

### 32K RAM

The CS1 signal determines when the 32K RAM bank is enabled. This occurs when the level of A15 matches that of jumper J1, and corresponds with either 0x0000 or 0x8000. When CS1 is active low, U9 is selected and the data bus buffer U1 is enabled.

### Mixed RAM, ROM Bank

The CS2 signal determines when the 32K mixed RAM/ROM band is enabled. A13, A14 and A15 are XOR'd with jumpers for each 8K bank to determine when selection occurs. All four bank selection signals are further XOR'd. When a bank is selected CS2 asserts active low enabling the data bus buffer U1. 

> **NOTE:**
Bypass capacitors (C3 - C19) are provided for all IC's on the board. These have never been populated in testing and no issues have been encountered. They remain on the board only because they *might* be useful to someone in some situation.

## Assembly ##

1. Wash the board in warm water and mild detergent before assembly to remove any oils and contamination from the plated surfaces. Blot dry between two clean paper towels and allow to fully dry.
1. Add the resistor array 
1. Add the IC sockets
1. Add the jumper headers
1. Add the filter capacitors
1. Remove flux residue with isopropyl alcohol or commercial flux remover, dry thoroughly
1. Insert the IC's
1. Configure jumpers

## Checkout ##

Power the board and verify that power and ground voltages are correct. Component-side power traces are connected +5V, solder-side power traces are connected to ground.

## Configuration ##

### 32K RAM ###

The 32K RAM bank is addressable at either 0x0000 or 0x8000 via J1. Most configurations will have this bank in low memory.

### 32K RAM/ROM ###

This bank provides up to 32K in 4 banks of 8K of either RAM or ROM. For each 8K bank, a jumper (J3, J7, J11, J13) selects whether the bank base address is in low memory (0x0000) or the upper 32K of memory (0x8000). Two jumpers per bank (J4/J5, J8/J9, J12/J13, J16/J17) selects one of four possible 8K bank offsets (0K, 8K, 16K, 24K) from the base address. A fourth jumper per bank (J2, J6, J10, J14) selects whether the bank supports read/write (RAM) or read-only (ROM).

Most any brand of 2764 EPROM, 28C64 EEPROM or 6164 Static RAM devices are supported. 2732 devices can also be used but the upper 4K of the bank is not addressable. Any combination of devices is allowed as long as they do not have conflicting addresses. 

> **Important:**
All memory cards in a system must propertly map their memory into the 1802's 64K space. Banks jumpered to addresses at the same location on cards within the system will result in conflicts and indeterminate behavior. **All** jumper selectable banks within the system that are empty or in need of being masked from selection must be jumpered to an unused address in the 1802 address space. Since masked addresses should never be accessed from a program, it _should_ be safe in this situation to jumper multiple boards to the same (unused/masked) address range.

Note that no provision has been made for selecting multiple CDP18S638 cards and thus enabling greater than 64K addressable memory in a system configuration.

## Legal Disclaimer, Warranty and Customer Support ##

Use of this product is without warranty of merchantability of fitness for any purpose other than learning and enjoyment!

I support and stand by my products. Email support is available should you have any questions or issues.  PCB's are warranted against defects and will be replaced free of charge. Kit IC's have a 30-day warranty from date of receipt. If you encounter any problems or have suggestions please contact me.

I sincerely hope you enjoy this retro computing creation and thanks again for your support!

Scott Pack/packtronix
somaspack@yahoo.com

<p style="page-break-after: always;">
</p>

## Appendix I - Parts List ##

| Component | Description | Source
| -- | ----------- | -----------
| C1, C2 | 22mf 25V radial electrolytic cap. | Jameco 93739
| J1 | single row 3-pin male header | Jameco 109576
| J2 - J5, J6 - J9, J10 - J13, J14 - J17 | dual row 12-pin male header | Jameco 203810	
| RN1 | 22Kohm x 8 resistor network	| Jameco 1971407
| U1 | 74HC245 | Jameco 45671
| U2, U3 | 74HC244 | Jameco 45655
| U4 |74HC573 | Jameco 46076
| U5, U8, U10 - U12 |74HC86 | Jameco 46181
| U6, U13, U14 | 74HC32 | Jameco 45794
| U7 | 74HC04 | Jameco 45209
| U9 | 62256 | surplus/eBay
| U15 - U18 | 6264/2764 | surplus/eBay

<p style="page-break-after: always;">
</p>

# Appendix II - Schematics ##

# 32K RAM #

<p style="page-break-after: always;">
    <a href="schematics/cosmac_32k_ram_nonbuffered_2.svg"><img src="schematics/cosmac_32k_ram_nonbuffered_2.svg" width="700" style="border:1px solid black" title="32K RAM" alt="32K RAM"></a>
</p>

# 32K RAM/ROM #

<p style="page-break-after: always;">
    <a href="schematics/cosmac_32k_ram_rom_8k_select_logic_nonbuffered_2.svg"><img src="schematics/cosmac_32k_ram_rom_8k_select_logic_nonbuffered_2.svg" width="450" style="border:1px solid black" title="32K RAM/ROM" alt="32K RAM/ROM"></a>
</p>

# Data and Address Buffering #

<p style="page-break-after: always;">
    <a href="schematics/cosmac_data_and_addess_buffering.svg"><img src="schematics/cosmac_data_and_addess_buffering.svg" width="700" style="border:1px solid black" title="Data and Address Logic/Buffering" alt="Data and Address Logic/Buffering"></a>
</p>

# COSMAC Microboard Bus #

<p style="page-break-after: always;">
    <a href="schematics/cosmac_expansion_44edge.svg"><img src="schematics/cosmac_expansion_44edge.svg" width="600" style="border:1px solid black" title="COSMAC Expansion" alt="COSMAC Expansion"></a>
</p>

## Appendix III - PCB

<a href="schematics/board-outline-net.svg"><img src="schematics/board-outline-net.svg" width="600" style="border:1px solid black" title="PCB Image" alt="PCB Image"></a>

