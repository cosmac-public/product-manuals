# About the Manuals


## Markdown

Unlike most all other Web content, the manuals here are not in HTML or PDF but in a simple text-encoding language called *markdown*, stored in plain text files with the *.md* extension. For Open Source projects, markdown is the standard way to write documentation. It's use is further encouraged by GitLab by providing their own extensions for software development. Markdown syntax is converted by the GitLab website into rich text (HTML) content when a file is opened for viewing. Due to it's ease of use and rich expression, markdown is widely supported in other manners.


## Web Viewing

Manuals can be viewed online from the GitLab website and the markdown will be converted to formatted text automatically. Just click on the MD file.

MD files can also be downloaded to your computer and viewed in a browser with a markdown extension installed (see below). 


## Printing to Paper

Manuals are optimized for printing on US Letter size paper, follow the following directions to get best results:

* For schematics best resolution will be found using a laser printer. Depending on the size of the circuit fine details may get lost with inkjet types.
* On the GitLab page click on the toolbar item _'Open raw'_ to view and print the manual without added content
* Manuals can also be downloaded and printed using a browser plugin for Markdown content


### Browser Markdown Extensions

Browsers do not natively know how to render Markdown content, an extension is required. Once installed, open the *.md file in your browser and it will be render to rich text (HTML).


#### Firefox

For Firefox  *GitLab Markdown Viewer* is an extension that has been tested.

* Open Preferences, click on _Extensions & Themes_
* Search for _'GitLab Markdown Viewer'_
* Open the search result
* Click 'Add to Firefox', and confirm access to all Websites
* Open any *.MD file and it will be converted to rich text


#### Chrome

For Chrome, the *Markdown Viewer* extension that has been tested. 

* Open the Chrome Web Store [https://chrome.google.com/webstore]
* Search for extension - _Markdown Viewer_
* Add to Chrome
* Once installed, open the extension from the toolbar
* Click on _Advanced Options_
* In the _Allowed Origins_, add _'https://gitlab.com/'_
* Open any *.MD file and it will be converted to rich text


## Schematics and PCB Images

All images of schematics and PCB's are Scalable Vector Graphic (SVG) format. Vector images provide excellent screen and print quality because they are scalable without loss of detail. This makes an ideal format for electronic delivery of the schematics such as the fine lines and text.
 
Original schematics were drawn using Linux gEDA suite tool - _gschem_, exported as EPS files and converted to SVG using Inkscape for Linux.

PCB images were exported from the manufacturing Gerber files as PostScript using Linux gEDA suite tool -  _gerbv_, then converted to SVG files using Inkscape for Linux.

Thumbnails are shown in the manuals. When clicked, the full-size SVG file is opened in the browser. All moderm browsers naturally know how to display SVG files without extensions. Use the browser to zoom into the schematic without losing any detail. SVG files can also be downloaded and printed in high resolution. 
