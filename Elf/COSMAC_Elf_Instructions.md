# COSMAC ELF MICROBOARD ASSEMBLY INSTRUCTIONS

__Version 1.1__

Thanks for purchasing my kit for a COSMAC Elf Microboard! This project began over 35 years ago when as a teenager I like so many other people built my first computer: a wire-wrapped Elf from Popular Electronics.

I've had a lot of fun designing a PCB board version of the Elf and making a few changes. The major change is I've upgraded the Elf to 1K RAM. This required the addition of an address latch and circuitry to de-bounce the MP switch. Functionally the circuit is the same as the original Elf.

With the addition of a card edge connector, the Elf can be used as the basis of an expanded system. All critical CPU signals are extended to the card edge. See section on Expansion for details.


## PCB


The PCB is double-sided FR4 material and plated in electro-less nickel immersion gold (ENIG). This provides good protection from oxidation on the edge card fingers.

You will find best results working on this board using a variable temperature soldering station and a micro soldering tip. In the need of desoldering, a powered vacuum desoldering gun such as the Hakko FR-301 is highly recommended.

> **NOTE:**
The solder pads on the board have very small profiles and are easily damaged by too high or prolonged heat which will cause them to lift from the board.


## Kit Contents

If you purchased a kit, it contains all the non-optional parts listed in the Parts List. To keep costs low and for personal preferences, IC sockets are provided only
for U1, U2 and U3.


# Configuration

Power to the board can be configured in several ways:

* Microboard Backplane - provides 5V regulated voltage via the card edge fingers, board does not require any additional (optional) components
* PWR1 Header - optional MTA-100 connector, but you can substitute other .100" headers or solder directly to the pads.

There are two voltage options when using the PWR1 Header:

* External regulated 5V - add optional components PWR1, C1
>Note: 
Jumper U15-1 to U15-3 (leave the center pin unconnected), this connects PWR1's +V to the board's +V
* External unregulated 9V - add optional components U15, PWR1, C1


## Assembly

1. Wash the board in warm water and mild detergent before assembly to remove any oils and contamination from the plated surfaces. Blot dry between two clean paper towels.
1. Decide how you will power the board (see above).
1. Add the crystal, resistors, diodes, capacitors.
>Note:
Check the orientation of C4 and C3 - they can be inserted 90-degrees the wrong direction.
1. Add the IC sockets.
1. Add the switches. The switches fit tight in the board and will mostly self-align but there is some movement. Expand the bracket pins to align and securely lock the switches. Before soldering all pins, confirm desired alignment.
1. If soldering IC's, complete the Checkout Procedure steps 1-4 before proceeding


## Checkout  Procedure

>Note:
You will need a digital logic probe and multi-meter to perform circuit checkout.

1. Clean the board of flux residue using 90% isopropyl alcohol or suitable commercial product.
1. Bottom side: Check that all pins have been soldered and that all joints are bright and shiny.
1. Top side: Double-check correct placement and orientation of ALL components.
1. Check voltage polarity is correct: connect the power source and verify using a multi-meter that IC1-40 shows 5V and IC1-20 shows 0V.
1. Insert all socketed IC's observing polarity.
1. Using a logic probe, check that IC1-39 shows a pulsing signal.
1. Set all toggles to the down position (logic LOW).
1. Set the data toggles to _'7B'_.
1. Press the INPUT pushbutton -> nothing happens.
1. Toggle LOAD to the up position, press INPUT --> _'7B'_ is displayed on the data displays.
1. Enter '30' on the data toggles. Press INPUT --> _'30'_ is displayed on the data displays.
1. Enter '00' on the data toggles. Press INPUT --> _'00'_ is displayed on the data displays.
1. Toggle LOAD to the down position. Toggle MP to the up position. Toggle LOAD to the up position. Press INPUT --> displays show _'7B'_.
1. Press INPUT again --> displays show _'30'_, pressing again shows _'00'_.
1. Set MP and LOAD down.
1. Set RUN up --> Q LED lights.

This concludes the diagnostic tests. If you encounter problems check the following:
* Did you carefully follow all of the previous steps? I thought the oscillator must be working... long wait to find the capacitors were inserted cross-wise.


## Expansion

The Elf is an extremely simple circuit and was not intended to be the basis of an expanded system. However with moderate care it can be used for such and was used to bootstrap development of my System-1 board. All 1802 signals are extended to the edge fingers with the exception of CLOCK. The Elf does not have external clock signal buffering and I did not add any. This should not impact expansion as there are rare needs for the system clock. Also be aware that WAIT and CLEAR are **not** isolated from the edge fingers and if you wish drive these signals from an expansion board (i.e. some other control circuit) you will need to make appropriate changes to the Elf to isolate them from the rest of the circuit.


## Legal Disclaimer, Warranty and Customer Support ##

Use of this product is without warranty of merchantability or fitness for any purpose other than learning and enjoyment!

I support and stand by my products. Email support is available should you have any questions or issues. PCB's are warranted against defects in manufacture and will be replaced free of charge. Kit IC's have a 30-day DOA warranty from date of receipt. If you encounter any problems or have suggestions please contact me.

I sincerely hope you enjoy this retro computing creation and thanks again for your support!

Scott Pack/packtronix

[somaspack@yahoo.com]

<p style="page-break-after: always;"></p>

# Appendix

## Appendix I - Parts List

| Schematic | Description | Supplier/PN
---|---|---
| C1 | CAP,RADIAL,22uF,50V (OPTIONAL) | Jameco/93739
|C2 | CAP,RADIAL,22uF,50V | Jameco/93739
|C3, C4 | CAP CER 18PF 50V 5% RADIAL | DigiKey/490-8653-ND
|D1-D6 | DIODE,1N914A, DO-35,100V,0.2A | Jameco/655269
|LED1 | MINIATURE T-1 (3MM) RED LED | AllElectronics/MLED-1
|PWR1 | CONN HEADER VERT 2POS .100 (OPTIONAL) | DigiKey/A19423-ND
|R1_7 | RES ARRAY 47K OHM 7 RES 8SIP | DigiKey/4308R-1-473LF-ND
|R8, R9 | RES 47K OHM 1/4W 5% | Jameco/691260
|R10 | RES 470 OHM 1/4W 5% |Jameco/690785
|R11 | RES 10M OHM 1/4W 5%  |Jameco/691817
|R20-21 | RES 22K OHM 1/4 W 5% | Jameco/691180
|S0-S7, S1_RUN, S2, LOAD, S3_MP | SWITCH TOGGLE SPDT | DigiKey/360-2614-ND
|S12_INPUT | SWITCH PUSH SPDT | DigiKey/360-2617-ND or 360-2556-ND
|U1 | CDP1802AE/ACE - CMOS 8-BIT MICROPROCESSOR | Various Surplus
|U2, U3 | 5114/21C14 1024x4 CMOS STATIC RAM | Various Surplus
|U4, U5 | CD4050BE - BUFF/CONVERTER HEX | DigiKey/296-2056-5-ND
|U6, U7 | TIL311 - HEXADECIMAL DISPLAY WITH LOGIC | Various Surplus
|U8, U9 | CD4016BE - QUAD BILATERAL SWITCH | DigiKey/296-2036-5-ND
|U10 | CD4023BE - GATE NAND 3CH 3-INP | DigiKey/296-2041-5-ND
|U11 | CD4049UBE - HEX BUFF/CONV INVERTER | DigiKey/296-2055-5-ND
|U12 | CD4013BE - D-TYPE POS TRG DUAL | DigiKey/296-2033-5-ND
|U13 | CD4042 - QUAD D LATCH | DigiKey/296-2049-5-ND
|U14 | CD4011BE - GATE NAND 4CH 2-INP | DigiKey/296-2031-5-ND
|U15 | REG 5V 1A TO220-3 (OPTIONAL) | DigiKey/LM7805ACT-ND
|- | HEATSINK TO-220 LOW HEIGHT BLK (OPTIONAL) | DigiKey/345-1023-ND
|XTAL1 | CRYSTAL 2.0000MHZ 18PF | DigiKey/CTX869-ND

<p style="page-break-after: always;"></p>

## Appendix II - Schematics

### CPU

<p style="page-break-after: always;">
    <a href="schematics/elf_cpu.svg"><img src="schematics/elf_cpu.svg" width="550" style="border:1px solid black"/></a>
</p>

### CPU Control

<p style="page-break-after: always;">
    <a href="schematics/elf_control_2.svg"><img src="schematics/elf_control_2.svg" width="700" style="border:1px solid black"/></a>
</p>

### Data Input

<p style="page-break-after: always;">
    <a href="schematics/elf_data_input.svg"><img src="schematics/elf_data_input.svg" width="700" style="border:1px solid black"/></a>
</p>

### Data Display

<p style="page-break-after: always;">
    <a href="schematics/elf_data_display.svg"><img src="schematics/elf_data_display.svg" width="700" style="border:1px solid black"/></a>
</p>

### Memory

<p style="page-break-after: always;">
    <a href="schematics/elf_ram_5114.svg"><img src="schematics/elf_ram_5114.svg" width="700" style="border:1px solid black"/></a>
</p>

### Microboard Expansion

<p style="page-break-after: always;">
    <a href="schematics/elf_expansion_44edge.svg"><img src="schematics/elf_expansion_44edge.svg" width="500" style="border:1px solid black"/></a>
</p>


## Appendix III - PCB

<a href="schematics/elf_pcb.svg"><img src="schematics/elf_pcb.svg" width="600" style="border:1px solid black"/></a>

